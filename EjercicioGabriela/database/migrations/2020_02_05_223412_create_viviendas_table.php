<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViviendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viviendas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('c_habit');
            $table->string('c_baños');
            $table->string('c_colonia');
            $table->float('precio',8,2);
            $table->float('tamanio',8,2);
            $table->string('municipio');
            $table->string('departamento');
            $table->string('categoria');
            $table->string('negociable');
            $table->string('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viviendas');
    }
}
